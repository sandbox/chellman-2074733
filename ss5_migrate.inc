<?php

/**
 * @file
 * Migration parent class, with methods everyone needs
 */

abstract class SSMigration extends XMLMigration {
  // Default location of XML file(s).  This is the directory above the Drupal root,
  // so it's probably not web-accessible.
  // (This can also be an URL instead of a file path.)
  var $xml_folder = '';

  public function __construct() {
    // Always call the parent constructor first for basic setup
    parent::__construct();
    
    // set a default path for the XML folder.
    $this->xml_folder = variable_get('ss5_migrate_xml_path', DRUPAL_ROOT . '/sites/default/files/xml');

    // With migrate_ui enabled, migration pages will indicate people involved in
    // the particular migration, with their role and contact info. We default the
    // list in the shared class; it can be overridden for specific migrations.
    $this->team = array(
      new MigrateTeamMember('You', 'you@example.com', t('Team Member')),
      new MigrateTeamMember('Me', 'me@example.org', t('Drupal Developer')),
    );
  }
  
  /**
   * Strip out HTML line breaks
   */
  protected function br2nl($string) {
		return preg_replace ('!<br\s*/?\s*>!', "\n", $string);
	}
	
	protected function unichr($i) {
    return iconv('UCS-4LE', 'UTF-8', pack('V', $i));
	}
	
	// tries to strip out anything that's outside the three byte UTF8 schema used by Drupal
	// (needs work)
	protected function cutToThreeByte($string) {
		$pattern = '/[' . $this->unichr(0x1F09E) .'-'. $this->unichr(0x1F5FF) .
				$this->unichr(0xE000) .'-'. $this->unichr(0xF8FF) . ']/u';
	
		return preg_replace($pattern, ' ', $string);
	}
}

/**
 * The SSUserMigration uses the giant XML file as source
 * and creates Drupal users as destination.
 */
class SSUserMigration extends SSMigration {
  public function __construct() {
    parent::__construct();
    $this->description = t('Squarespace Users');

    // There isn't a consistent way to automatically identify appropriate "fields"
    // from an XML feed, so we pass an explicit list of source fields    
    $fields = array(
      /*
      // Squarespace exports these, can use them if you want
      'first_name' => t('First Name'),
      'last_name' => t('Last Name'),
      'display_name' => t('Display Name'),
      */
      'login' => t('Login'),
      'email' => t('Email'),
      'deleted' => t('Deleted'), // probably ignore these if deleted = true
      'is_confirmed' => t('Confirmed'), // probably ignore these if is-confirmed = false
      // 'is-enabled' => t('Enabled'), // probably ignore these if is-enabled = false (seems to be always true)
      'encrypted_password' => t('Password'),
      'created_on' => t('Created On'),
    );
    
    // The source ID here is the one retrieved from each data item in the XML file, and
    // used to identify specific items
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'id' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
        )
      ),
      MigrateDestinationUser::getKeySchema()
    );
    
    $items_url = $this->xml_folder . 'ss-accounts.xml';
    $item_xpath = '//website-member-account';  // relative to document
    $item_ID_xpath = 'id';         // relative to item_xpath and gets assembled
                                   // into full path /squarespace-wireframe/website-member-accounts/website-member-account/id

    $items_class = new MigrateItemsXML($items_url, $item_xpath, $item_ID_xpath);
    $this->source = new MigrateSourceMultiItems($items_class, $fields);

    $this->destination = new MigrateDestinationUser();

    $this->addFieldMapping('name', 'login')
         ->xpath('login');
    $this->addFieldMapping('mail', 'email')
         ->xpath('email');
    $this->addFieldMapping('pass', 'encrypted_password')
         ->xpath('encrypted-password');
    $this->addFieldMapping('created', 'created_on')
         ->xpath('created-on'); // transform this later
    /*
    
    // name fields you can use if you add them to your user entities
    
    $this->addFieldMapping('field_name_display', 'display_name')
         ->xpath('display-name');
    $this->addFieldMapping('field_first_name', 'first_name')
         ->xpath('first-name');
    $this->addFieldMapping('field_last_name', 'last_name')
         ->xpath('last-name');
    */
	  $this->addFieldMapping('status', 'is_confirmed')
         ->xpath('is-confirmed');
    $this->addUnmigratedDestinations(array('theme', 'signature', 'signature_format', 'access', 'login', 'timezone', 'language', 'picture', 'init', 'data', 'roles', 'role_names', 'is_new', 'path'));

	}
	
	public function prepareRow($row) {
		// skip deleted users entirely
		if ($row->xml->deleted == 'true') {
			return FALSE;
		}
		
		// you can short-circuit this migration and inspect this item by running the migration in drush and exiting.  The drush command is:
		// drush mi SSUser --limit="1 item"
		// The --limit switch is redundant, but helps make totally sure you don't run this over many rows accidentally.
		
		//print_r($row);
		//exit;
	}
	
	public function prepare($account, $row) {
		// convert anything base64 encoded
		
		// if using the name fields, you'll probably need to base64 decode them
		/*
		if ($row->xml->{'display-name'}['encoding'] == 'base64') {
			$account->field_name_display[LANGUAGE_NONE][0]['value'] = base64_decode($row->display_name);
		}
		
		if ($row->xml->{'first-name'}['encoding'] == 'base64') {
			$account->field_first_name[LANGUAGE_NONE][0]['value'] = base64_decode($row->first_name);
		}
		
		if ($row->xml->{'last-name'}['encoding'] == 'base64') {
			$account->field_last_name[LANGUAGE_NONE][0]['value'] = base64_decode($row->last_name);
		}
		*/
	
		$account->status = ($account->status == 'true') ? 1 : 0;
		
		// same short-circuiting style debugging here.  This runs after prepareRow, so you could just put this in here.
		// print_r($row);
		// print_r($account);
		// exit;
	}
}

/**
 * The SSPostsMigration uses the giant XML file as source
 * and creates Drupal article nodes as the destination.
 */
class SSPostsMigration extends SSMigration {
  public function __construct() {
    parent::__construct();
    $this->description = t('Squarespace Tips');
    $this->dependencies = array('SSUser');

    // Your Squarespace site might not have all these fields.  Check your XML.
    $fields = array(
      'title' => t('Title'),
      'subtitle' => t('Subtitle'),
      'excerpt' => t('Excerpt'),
      'body' => t('Body'), // base64 encoded
      'registered_author_id' => t('Author ID'),
      'updated_on' => t('Changed'),
      'added_on' => t('Created On'),
      'allow_comments' => t('Comment status'),
      'url_id' => t('URL Path'),
      'sticky' => t('Sticky'),
      'published' => t('Published'),
    );
    
    // The source ID here is the one retrieved from each data item in the XML file, and
    // used to identify specific items
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'id' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
        )
      ),
      MigrateDestinationNode::getKeySchema()
    );
    
    $items_url = $this->xml_folder . '/ss-tips.xml';
    $item_xpath = '//journal-entry';
    $item_ID_xpath = 'id';

    $items_class = new MigrateItemsXML($items_url, $item_xpath, $item_ID_xpath);
    $this->source = new MigrateSourceMultiItems($items_class, $fields);

    // change 'article' to your content type's machine name as appropriate
    $this->destination = new MigrateDestinationNode('article');

    $this->addFieldMapping('title', 'title')
         ->xpath('title');
    $this->addFieldMapping('field_subtitle', 'subtitle')
         ->xpath('subtitle');
    $this->addFieldMapping('field_subtitle:language')
         ->defaultValue(LANGUAGE_NONE);
    $this->addFieldMapping('body', 'body')
         ->xpath('body');
    $this->addFieldMapping('body:summary', 'excerpt')
         ->xpath('excerpt');
    $this->addFieldMapping('body:format')
         ->defaultValue('full_html');
    $this->addFieldMapping('uid', 'registered_author_id')
         ->xpath('registered-author-id')
         ->sourceMigration('SSUser')
         ->defaultValue(1);
    $this->addFieldMapping('created', 'added_on')
         ->xpath('added-on');
    $this->addFieldMapping('changed', 'updated_on')
         ->xpath('updated-on');
    $this->addFieldMapping('comment', 'allow_comments')
         ->xpath('allow-comments');
    $this->addFieldMapping('sticky', 'sticky')
         ->xpath('sticky');
    $this->addFieldMapping('status', 'published')
         ->xpath('published');
    
    if (module_exists('path')) {
        $this->addFieldMapping('path', 'url_id')
         ->xpath('url-id');
    }
         
    $this->addUnmigratedDestinations(array('promote', 'revision', 'log', 'language', 'tnid', 'translate', 'revision_uid', 'is_new', 'body:language', 'field_tags', 'field_image'));

	}
	
	public function prepareRow($row) {
		// skip unpublished entries
		if ($row->xml->published == 'false') {
			return FALSE;
		}
		
		// followup posts have parent ids.  skip them for now.
		if ($row->xml->{'parent-id'}) {
			return FALSE;
		}
	}
	
	public function prepare($node, $row) {
		// convert anything base64 encoded
		if ($row->xml->{'body'}['encoding'] == 'base64') {
			$node->body[LANGUAGE_NONE][0]['value'] = base64_decode($row->body);
		}
		if ($row->xml->title['encoding'] == 'base64') {
			$node->title = base64_decode($row->xml->title);
		}
		if ($row->xml->{'excerpt'}['encoding'] == 'base64') {
			$node->body[LANGUAGE_NONE][0]['summary'] = base64_decode($row->excerpt);
		}
		
		//  try to strip high byte emoji
		$node->body[LANGUAGE_NONE][0]['value'] = $this->cutToThreeByte($node->body[LANGUAGE_NONE][0]['value']);
		$node->body[LANGUAGE_NONE][0]['summary'] = $this->cutToThreeByte($node->body[LANGUAGE_NONE][0]['summary']);
		
		// comment status
		$node->comment = ($row->allow_comments == 'true') ? COMMENT_NODE_OPEN : COMMENT_NODE_CLOSED;
		
		// other stati
		$node->sticky = ($row->sticky == 'true') ? NODE_STICKY : NODE_NOT_STICKY;
		$node->status = ($row->published == 'true') ? NODE_PUBLISHED : NODE_NOT_PUBLISHED;
		
		// dates
		$node->created = strtotime($row->added_on);
		$node->changed = strtotime($row->updated_on);
	}
}

/**
 * The SSCommentsMigration uses the giant XML file as source
 * and creates Drupal comments as the destination.
 *
 * This one's tricky due to the weird setup of the Squarespace exported data.
 */
class SSPostsCommentsMigration extends SSMigration {
  public function __construct() {
    parent::__construct();
    $this->description = t('Squarespace Tips Comments');
    $this->dependencies = array('SSUser', 'SSPosts');

    // There isn't a consistent way to automatically identify appropriate "fields"
    // from an XML feed, so we pass an explicit list of source fields    
    $fields = array(
      'body' => t('Body'), // base64 encoded
      'registered_author_id' => t('Author ID'),
      'added_on' => t('Created On'),
    );
    
    // The source ID here is the one retrieved from each data item in the XML file, and
    // used to identify specific items
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'id' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
        )
      ),
      MigrateDestinationComment::getKeySchema()
    );
    
    $items_url = $this->xml_folder . '/ss-tips-comments.xml';
    $item_xpath = '//comment';
    $item_ID_xpath = 'id';

    $items_class = new MigrateItemsXML($items_url, $item_xpath, $item_ID_xpath);
    $this->source = new MigrateSourceMultiItems($items_class, $fields);

    // use the machine ID of the content type these comment belong to,
    // e.g. comment_node_MACHINENAME
    $this->destination = new MigrateDestinationComment('comment_node_article');
    
    $this->addFieldMapping('comment_body', 'body')
         ->xpath('body');
    $this->addFieldMapping('comment_body:format')
         ->defaultValue('filtered_html');
    $this->addFieldMapping('uid', 'registered_author_id')
         ->xpath('registered-author-id')
         ->sourceMigration('SSUser')
         ->defaultValue(1);
    $this->addFieldMapping('created', 'added_on')
         ->xpath('added-on');
    $this->addFieldMapping('subject')
         ->defaultValue('');
    $this->addFieldMapping('status', 'published')
         ->xpath('published')
         ->defaultValue(COMMENT_PUBLISHED);
         
    $this->addUnmigratedDestinations(array('pid', 'changed', 'hostname', 'name', 'mail', 'homepage', 'language', 'thread', 'comment_body:language'));
    
    // add an additional SimpleXML object to handle the joins
    $this->joinXML = simplexml_load_file($this->xml_folder . '/ss-comments-join.xml');
    // and a MigrateSQLMap object to handle the lookup of the new node IDs for the tips
		$this->joinMap = new MigrateSQLMap('SSPosts',
      array(
        'id' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
        )
      ),
      MigrateDestinationNode::getKeySchema()
    );
	}
	
	public function prepareRow($row) {
		// skip unapproved comments for now
		if ($row->xml->approved == 'false') {
			return FALSE;
		}
	}
	
	public function prepare($comment, $row) {
		// get the nid for this comment
		list($nid_search_result) = $this->joinXML->xpath('//journal-entry-comment[comment-id='. $row->id .']/journal-entry-id');
		
		// $nid_search_result is a SimpleXMLElement, which is array-like for the purpose of retrieving values,
		// but is really an object.  So we take the returned value and feed it into the array that lookupDestinationID() expects.
		$nid_lookup = $this->joinMap->lookupDestinationID(array($nid_search_result[0]));
		$comment->nid = $nid_lookup['destid1'];
		
		// convert anything base64 encoded
		if ($row->xml->{'body'}['encoding'] == 'base64') {
			$comment->comment_body[LANGUAGE_NONE][0]['value'] = base64_decode($row->body);
		}
		
		// dates
		$comment->created = strtotime($row->added_on);
		
		drush_log('Added comment to node ' . $comment->nid, 'ok');
	}
}



/**
 * The SSForumMigration uses the giant XML file as source
 * and creates Drupal nodes as the destination.
 */
class SSForumMigration extends SSMigration {
  public function __construct() {
    parent::__construct();
    $this->description = t('Squarespace Forum Nodes');
    $this->dependencies = array('SSUser');

    // the only fields available so far are the IDs
    $fields = array(
      'id' => t('Legacy ID'),
      'comment_id' => t('Legacy Comment ID'),
      'module_id' => t('Module ID'),
      'added_on' => t('Added On'),
      'registered_author_id' => t('Author ID'),
      'body' => t('Body'), // base64?
      'title' => t('Title'), // base64?
      'approved' => t('Approved'),
    );
    
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'id' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
        )
      ),
      MigrateDestinationNode::getKeySchema()
    );
    
    $items_url = $this->xml_folder . '/ss-forum.xml';
    $item_xpath = '//discussion-post';
    $item_ID_xpath = 'id';

    $items_class = new MigrateItemsXML($items_url, $item_xpath, $item_ID_xpath);
    $this->source = new MigrateSourceMultiItems($items_class, $fields);

    $this->destination = new MigrateDestinationNode('forum');

	  // set these all up with nothing in them, because that's all we can do
    $this->addFieldMapping('title', 'title')
         ->defaultValue('Forum post');
    $this->addFieldMapping('body', 'body')
         ->defaultValue('');
    $this->addFieldMapping('body:format')
         ->defaultValue('full_html');
    $this->addFieldMapping('uid', 'registered_author_id')
         ->defaultValue(1);
    $this->addFieldMapping('created', 'added_on');
    $this->addFieldMapping('comment', 'approved');
    $this->addFieldMapping('status', 'approved');
         
    // taxonomy
    $this->addFieldMapping('taxonomy_forums')
         ->defaultValue('General discussion'); // this will be updated below
         
    $this->addUnmigratedDestinations(array('promote', 'revision', 'log', 'language', 'tnid', 'translate', 'revision_uid', 'is_new', 'body:language', 'body:summary', 'sticky', 'changed', 'path', 'taxonomy_forums:source_type', 'taxonomy_forums:create_term', 'taxonomy_forums:ignore_case'));
    
    // create a separate XML reader for the forum content
    // because the content is not included in the original element
    $items_url2 = $this->xml_folder . '/ss-comments-content.xml';
    $item_xpath2 = '//comment';
    $this->commentLookup = new MigrateItemsXML($items_url2, $item_xpath2, $item_ID_xpath);
    
    // get the map for users
    // because the discussion-post element doesn't have a registered-author-id.
    // which is DUMB.
		$this->joinMap = new MigrateSQLMap('SSUser',
      array(
        'id' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
        )
      ),
      MigrateDestinationUser::getKeySchema()
    );
	}
	
	public function prepareRow($row) {
	
		$commentID = (int)$row->xml->{'comment-id'};
		$commentXML = $this->commentLookup->getItem($commentID);
		
		$row->title = $commentXML->xml->{'title'};
		$row->body = $commentXML->xml->{'body'};
		$row->added_on = (string)$commentXML->xml->{'added-on'};
		$row->approved = (bool)$commentXML->xml->{'approved'};
		
		
		$row->registered_author_id = (int)$commentXML->xml->{'registered-author-id'};
		$uid_lookup = $this->joinMap->lookupDestinationID(array($row->registered_author_id));
		$row->uid = $uid_lookup['destid1'];
	}
	
	public function prepare($node, $row) {
		// Set the correct forum based on module ID.
		// I've set these in advance by finding the module IDs in the XML file.
		// You could, of course, migrate these from the XML directly using the module IDs.
		$forum = 'Forum 4';
		$tid = 4;
		
		switch($row->xml->{'module-id'}) {
			case '1' :
				$forum = 'General Discussion';
				$tid = 1;
				break;
			case '2' :
				$forum = 'Forum 2';
				$tid = 2;
				break;
			case '3' :
				$forum = 'Forum 3';
				$tid = 3;
				break;
			default :
				break;
		}
		$node->taxonomy_forums[LANGUAGE_NONE][0]['tid'] = $tid;
		
		// convert anything base64 encoded
		if ($row->body['encoding'] == 'base64') {
			$node->body[LANGUAGE_NONE][0]['value'] = base64_decode($row->body);
		}
		if ($row->title['encoding'] == 'base64') {
			$node->title = base64_decode($row->title);
		}
		
		// some posts could come through with no titles.
		if (empty($node->title)) $node->title = 'Post ID ' . $row->xml->{'id'};
		
		// dates
		$node->created = strtotime($row->added_on);
		
		// flags
		$node->comment = ($node->comment == 'true') ? COMMENT_NODE_OPEN : COMMENT_NODE_CLOSED;
		$node->status = ($node->status == 'true') ? NODE_PUBLISHED : NODE_NOT_PUBLISHED;
		
		$node->uid = $row->uid;
	}
}

/**
 * The SSForumCommentsMigration uses the giant XML file as source
 * and creates Drupal comments as the destination.
 */
class SSForumCommentsMigration extends SSMigration {
  public function __construct() {
    parent::__construct();
    $this->description = t('Squarespace Forum Comments');
    $this->dependencies = array('SSUser', 'SSForum');

    $fields = array(
      'id' => t('Legacy ID'),
      'comment_id' => t('Legacy Comment ID'),
      'parent_id' => t('Parent Legacy Post ID'),
      'module_id' => t('Module ID'),
      'added_on' => t('Added On'),
      'registered_author_id' => t('Author ID'),
      'body' => t('Body'), // base64?
      'title' => t('Title'), // base64?
      'approved' => t('Approved'),
    );
    
    // The source ID here is the one retrieved from each data item in the XML file, and
    // used to identify specific items
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'id' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
        )
      ),
      MigrateDestinationComment::getKeySchema()
    );
    
    $items_url = $this->xml_folder . '/ss-comments-posts.xml';
    $item_xpath = '//discussion-post';
    $item_ID_xpath = 'id';

    $items_class = new MigrateItemsXML($items_url, $item_xpath, $item_ID_xpath);
    $this->source = new MigrateSourceMultiItems($items_class, $fields);

    $this->destination = new MigrateDestinationComment('comment_node_forum');
    
    $this->addFieldMapping('nid', 'parent_id')
    		 ->xpath('parent-id')
    		 ->sourceMigration('SSForum')
    		 ->defaultValue(1);
    $this->addFieldMapping('comment_body', 'body');
    $this->addFieldMapping('comment_body:format')
         ->defaultValue('filtered_html');
    $this->addFieldMapping('uid', 'registered_author_id')
         ->defaultValue(1);
    $this->addFieldMapping('created', 'added_on');
    $this->addFieldMapping('subject')
         ->defaultValue('');
    $this->addFieldMapping('status', 'approved')
         ->defaultValue(COMMENT_PUBLISHED);
         
    // legacy lookup fields
		// $this->addFieldMapping('field_legacy_post_id', 'id');
		// $this->addFieldMapping('field_legacy_comment_id', 'comment_id')
		// 	->xpath('comment-id');
         
    $this->addUnmigratedDestinations(array('pid', 'changed', 'hostname', 'name', 'mail', 'homepage', 'language', 'thread', 'comment_body:language'));
    
    // create a separate XML reader for comment content because the base element doesn't include it
    $items_url2 = $this->xml_folder . '/ss-comments-content.xml';
    $item_xpath2 = '//comment';
    $this->commentLookup = new MigrateItemsXML($items_url2, $item_xpath2, $item_ID_xpath);
    
    // get the map for users
    // again, because the original doesn't have this information
		$this->userMap = new MigrateSQLMap('SSUser',
      array(
        'id' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
        )
      ),
      MigrateDestinationUser::getKeySchema()
    );
	}
	
	public function prepareRow($row) {
	
		$commentID = (int)$row->xml->{'comment-id'};
		$commentXML = $this->commentLookup->getItem($commentID);
		
		$row->approved = (bool)$commentXML->xml->{'approved'};
	
		// skip unapproved comments for now
		if (!$row->approved) {
			return FALSE;
		}
		
		$row->subject = $commentXML->xml->{'title'};
		$row->body = $commentXML->xml->{'body'};
		$row->added_on = (string)$commentXML->xml->{'added-on'};
		
		$row->registered_author_id = (int)$commentXML->xml->{'registered-author-id'};
		$uid_lookup = $this->userMap->lookupDestinationID(array($row->registered_author_id));
		$row->uid = $uid_lookup['destid1'];
	}
	
	public function prepare($comment, $row) {
	
		// convert anything base64 encoded
		if ($row->xml->{'body'}['encoding'] == 'base64') {
			$comment->comment_body[LANGUAGE_NONE][0]['value'] = base64_decode($row->body);
		}
		
		// convert anything base64 encoded
		if ($row->body['encoding'] == 'base64') {
			$comment->comment_body[LANGUAGE_NONE][0]['value'] = base64_decode($row->body);
		}
		if ($row->subject['encoding'] == 'base64') {
			$comment->subject = base64_decode($row->subject);
		}
		
		// dates
		$comment->created = strtotime($row->added_on);
		
		// user
		$comment->uid = $row->uid;
	}
}

class SSPostsFollowupMigration extends SSMigration {
  public function __construct() {
    parent::__construct();
    $this->description = t('Squarespace Tips (Followups)');
    $this->dependencies = array('SSUser', 'SSPosts');
    
    // we're updating nodes, so make the destination the system of record
    $this->systemOfRecord = Migration::DESTINATION;

    $fields = array(
      'title' => t('Title'),
      'subtitle' => t('Subtitle'),
      'excerpt' => t('Excerpt'),
      'body' => t('Body'), // base64 encoded
      'registered_author_id' => t('Author ID'),
      'updated_on' => t('Changed'),
      'added_on' => t('Created On'),
      'allow_comments' => t('Comment status'),
      'url_id' => t('URL Path'),
      'sticky' => t('Sticky'),
      'published' => t('Published'),
      'parent_id' => t('Parent ID'),
    );
    
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'id' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
        )
      ),
      MigrateDestinationNode::getKeySchema()
    );
    
    $items_url = $this->xml_folder . '/ss-tips-followups.xml';
    $item_xpath = '//journal-entry';
    $item_ID_xpath = 'id';

    $items_class = new MigrateItemsXML($items_url, $item_xpath, $item_ID_xpath);
    $this->source = new MigrateSourceMultiItems($items_class, $fields);

    $this->destination = new MigrateDestinationNode('post');

		$this->addFieldMapping('nid', 'parent_id')
			 	 ->xpath('parent-id')
				 ->sourceMigration('SSPosts');
	}
	
	public function prepare($node, $row) {
	
		// pull the current body value for the post of interest
		$field_array = db_select('field_revision_body', 'f')
			->fields('f', array('entity_id', 'body_value'))
			->condition('entity_type', 'node')
			->condition('bundle', 'post')
			->condition('entity_id', array($node->nid), 'IN')
			->condition('deleted', 0)
			->execute()
			->fetchAllKeyed();
		$bodyValue = array_pop($field_array);
		
		
		// take the followup and make an HTML block of stuff from it
		$date = format_date(strtotime($row->xml->{'added-on'}));
		$user = user_load($node->uid);
		$userName = $user->field_name_display[LANGUAGE_NONE][0]['safe_value'];
		
		$updateString = '<div class="update-meta">Update on <span class="date">' . $date . '</span> by <span class="user">' . $userName . '</span></div>';
	
		// convert anything base64 encoded
		if ($row->xml->{'body'}['encoding'] == 'base64') {
			$updateBody = base64_decode($row->xml->{'body'});
		} else {
			$updateBody = $row->xml->{'body'};
		}
		
		$updateBlock = "\n\n<div class='post-update'>\n";
		$updateBlock .= $updateString . "\n" . $updateBody . "\n";
		$updateBlock .= "</div>\n";
		
		$node->body[LANGUAGE_NONE][0]['format'] = 'full_html';
		$node->body[LANGUAGE_NONE][0]['value_format'] = 'full_html';
		$node->body[LANGUAGE_NONE][0]['value'] = $bodyValue . $updateBlock;
		
	  drush_log('Updated node ' . $node->nid, 'ok');
	}
}
