This is an example module demonstrating how migrations from Squarespace 5 into Drupal can work.

Notes:

- In your settings.php, set a conf variable for the location of the XML files, e.g. 

$conf['ss5_migrate_xml_path'] = DRUPAL_ROOT . '/sites/default/files/xml';

